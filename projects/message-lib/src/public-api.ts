/*
 * Public API Surface of message-lib
 */


export * from './lib/message-lib.module';

export * from './lib/msg-add/msg-add.component';
export * from './lib/msg-list/msg-list.component';
export * from './lib/msg-update/msg-update.component';
export * from './lib/msg-delete/msg-delete.component';

export * from './lib/snackbar/snackbar.component';
export * from './lib/MsgService/msg.service';


