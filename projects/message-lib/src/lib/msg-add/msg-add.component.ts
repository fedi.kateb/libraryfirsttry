import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MsgService } from '../MsgService/msg.service';
import { SnackbarComponent } from '../snackbar/snackbar.component';
@Component({
  selector: 'app-msg-add',
  templateUrl: './msg-add.component.html',
  styleUrls: ['./msg-add.component.css']
})
export class MsgAddComponent implements OnInit {
  msg;
  lang_Id: string;
  campany_Id: string;
  application_Id: string;
  module_Id: string;
  message_Code: string
  message_Content: string;
  constructor(private msgService: MsgService, private SnackbarComponent: SnackbarComponent,
    public dialogRef: MatDialogRef<MsgAddComponent>) { }
  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

 
  async add(event: MouseEvent) {
    const message = {
      Msg_Code: {
        'lang_Id': this.lang_Id,
        'campany_Id': this.campany_Id,
        'application_Id': this.application_Id,
        'module_Id': this.module_Id,
        'message_Code': this.message_Code
      },
      'msg_Content': this.message_Content
    }
    await this.msgService.addMessage(message).subscribe(
      response => {
        this.SnackbarComponent.openSnackBar('message is already added', 'cancel')
        this.dialogRef.close();
      },
      error => {
        this.msg = error.error.msg;
        this.SnackbarComponent.openSnackBar(error.error.msg, 'cancel')
      });
  }




  cancel(){
		this.dialogRef.close()
	  }

}
