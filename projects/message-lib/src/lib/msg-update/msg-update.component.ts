import { Component, OnInit, Input, Inject } from '@angular/core';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { MsgService } from '../MsgService/msg.service';
import { message } from '../Model/message';


@Component({
  selector: 'app-msg-update',
  templateUrl: './msg-update.component.html',
  styleUrls: ['./msg-update.component.css']
})
export class MsgUpdateComponent implements OnInit {
  lang_Id: string;
  campany_Id: string;
  application_Id: string;
  module_Id: string;
  message_Code: string;
  message_Content: string;


  constructor(public dialogRef: MatDialogRef<MsgUpdateComponent>,private SnackbarComponent :SnackbarComponent,
    private msgService : MsgService,
    @Inject(MAT_DIALOG_DATA) public data: message ) { 
     
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {

    this.lang_Id = this.data.lang_Id;
    this.campany_Id =  this.data.campany_Id;
    this.application_Id =  this.data.application_Id;
    this.module_Id = this.data.module_Id;
    this.message_Code =  this.data.message_Code;
    this.message_Content =  this.data.msg_Content;

  }


  update (){
    const message = {
      Msg_Code: {
        'lang_Id': this.lang_Id,
        'campany_Id': this.campany_Id,
        'application_Id': this.application_Id,
        'module_Id': this.module_Id,
        'message_Code': this.message_Code
      },
      'msg_Content': this.message_Content
    }
    
    if(this.message_Content != this.data.msg_Content ) {

    this.msgService.updateMessage(message) .subscribe(
      response => {
      
    this.SnackbarComponent.openSnackBar('message is already updated', 'cancel')
    this.dialogRef.close();
      },
      error => {
       
    this.SnackbarComponent.openSnackBar(error.error.msg, 'cancel')
      });


   
      
    }  else {
      this.dialogRef.close();
     
    }

  }
 

cancel(){
  this.dialogRef.close()
}


}
