import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MsgUpdateComponent } from './msg-update.component';

describe('MsgUpdateComponent', () => {
  let component: MsgUpdateComponent;
  let fixture: ComponentFixture<MsgUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MsgUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MsgUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
