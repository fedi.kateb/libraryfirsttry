import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MsgService {

  constructor(public http: HttpClient) { }
  messages: any[] = [];

   addMessage(message)  {
  return  this.http.post('http://127.0.0.1:8080/message', message)
     
  }


  getmessages() {
    return this.http.get<any[]>('http://127.0.0.1:8080/message');
  }



  updateMessage(message) {
    return this.http.put('http://127.0.0.1:8080/message', message)
     
  }




  desactiver(id : string) {
   return  this.http.delete('http://127.0.0.1:8080/message?id='+id)
     
  }


  activate(id : string) {
   return this.http.get('http://127.0.0.1:8080/activatemessage?id='+id)
    
  }



}
