import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MsgDeleteComponent } from './msg-delete.component';

describe('MsgDeleteComponent', () => {
  let component: MsgDeleteComponent;
  let fixture: ComponentFixture<MsgDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MsgDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MsgDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
