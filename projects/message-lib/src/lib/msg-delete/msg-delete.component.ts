import { Component, OnInit, Inject } from '@angular/core';
import { MsgService } from '../MsgService/msg.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SnackbarComponent } from '../snackbar/snackbar.component';
@Component({
  selector: 'app-msg-delete',
  templateUrl: './msg-delete.component.html',
  styleUrls: ['./msg-delete.component.css']
})
export class MsgDeleteComponent implements OnInit {
  status:string;
  constructor(public dialogRef: MatDialogRef<MsgDeleteComponent>,
    private SnackbarComponent:SnackbarComponent, 
    private msgService : MsgService,
    @Inject(MAT_DIALOG_DATA) public data ) { }


  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
     
    this.status= this.data.status
   }



 
   desactivate(){
    this.msgService.desactiver(this.data.id).subscribe(
      response => {
        this.SnackbarComponent.openSnackBar('message is already desactivated', 'cancel')   
        this.dialogRef.close();
      },
      error => {
        this.SnackbarComponent.openSnackBar(error.error.msg, 'cancel')  
        this.dialogRef.close();

      });
  }
  




  activate (){
    this.msgService.activate(this.data.id)  .subscribe(
      response => {
        this.SnackbarComponent.openSnackBar('message is already activated', 'cancel')   
        this.dialogRef.close();
      },
      error => {
        this.SnackbarComponent.openSnackBar(error.error.msg, 'cancel')   
        this.dialogRef.close();
      });
   
  }

cancel(){
  this.dialogRef.close()
}


}

