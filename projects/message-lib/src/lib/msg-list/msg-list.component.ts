import { Component, ViewChild, OnInit, AfterViewInit, AfterViewChecked } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MsgService } from '../MsgService/msg.service';
import { map } from 'rxjs/operators';
import { MsgAddComponent } from '../msg-add/msg-add.component';
import { MsgUpdateComponent } from '../msg-update/msg-update.component';
import { MsgDeleteComponent } from '../msg-delete/msg-delete.component';
import { message } from '../Model/message';

@Component({
  selector: 'app-msg-list',
  templateUrl: './msg-list.component.html',
  styleUrls: ['./msg-list.component.css']
})
export class MsgListComponent implements OnInit {
  nbr_message: Number;
  empty:boolean;
  messages: any[] = [];
  loading : boolean =true ;
  constructor(public dialog: MatDialog, private msgService: MsgService) {
    
  }
  
  openDialog(): void {
    const dialogRef = this.dialog.open(MsgAddComponent, { width: '1000px' });
    dialogRef.afterClosed().subscribe(result => {
      this.messages = [];
      this.getmessages();
    });
  }

  displayedColumns: string[] =
    ['campany_Id', 'application_Id', 'module_Id',
      'lang_Id', 'message_Code', 'msg_Content', 'status','status2'];
  dataSource: MatTableDataSource<message>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    console.log('1111')
    this.loading = true; 
    console.log(this.loading)
   this.getmessages();


   }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) { this.dataSource.paginator.firstPage();
       }

    
  }


  async display() {
    this.dataSource = new MatTableDataSource(this.messages);
    this.dataSource.paginator = await this.paginator;
    this.dataSource.sort = await this.sort;
   
  }


  async getmessages() {
  
    await this.msgService.getmessages().pipe(map(data => data))
      .subscribe(
        restItems => {
          if(restItems.length === 0){ this.empty = true }
          else{ this.empty=false}          
          restItems.forEach(element => {
            const msg = {
              'campany_Id': element.Msg_Code.campany_Id,
              'application_Id': element.Msg_Code.application_Id,
              'module_Id': element.Msg_Code.module_Id,
              'lang_Id': element.Msg_Code.lang_Id,
              'message_Code': element.Msg_Code.message_Code,
              'msg_Content': element.msg_Content,
              'status': element.status,
              'id': element._id
            }
            this.messages.push(msg)
          });
          this.messages.reverse();
          this.display()
          this.nbr_message = this.messages.length;
       
        });
        setTimeout(()=>{ this.loading = false; }, 3000);
        
  }


  openupdateDialog(campany_Id: string, application_Id: string, module_Id: string, 
    lang_Id : string, message_Code : string, msg_Content: string, status: string): void {
   
    const dialogRef = this.dialog.open(MsgUpdateComponent,{ 
      data : { campany_Id : campany_Id, application_Id : application_Id,
      module_Id : module_Id, lang_Id :lang_Id , 
      message_Code :message_Code , msg_Content :msg_Content,
      status : status}
    
    } );
    dialogRef.afterClosed().subscribe(result => {

      this.messages = [];
      this.getmessages();
   
    });
  }






openConfirDeleteDialog(id:string , status : string ): void {
  const dialogRef = this.dialog.open(MsgDeleteComponent, { data: {id:id , status: status} });
  dialogRef.afterClosed().subscribe(result => {
    this.messages = [];
    this.getmessages();
  });
}

}
