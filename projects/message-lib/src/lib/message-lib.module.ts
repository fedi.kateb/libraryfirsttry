
import { NgModule } from '@angular/core';
import { MsgListComponent } from './msg-list/msg-list.component';
import { MsgAddComponent } from './msg-add/msg-add.component';
import { MsgUpdateComponent } from './msg-update/msg-update.component';
import { MsgDeleteComponent } from './msg-delete/msg-delete.component';
import { HttpClientModule } from '@angular/common/http';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import { MsgService } from './MsgService/msg.service';
import { SnackbarComponent } from './snackbar/snackbar.component';

@NgModule({
  declarations: [MsgListComponent,
    MsgAddComponent,
    MsgUpdateComponent,
    MsgDeleteComponent,SnackbarComponent],
    imports: [
      BrowserModule,FormsModule
      ,HttpClientModule,
      BrowserAnimationsModule,MaterialModule,ReactiveFormsModule
    ], exports: [
      BrowserModule,
      BrowserAnimationsModule,MaterialModule
    ],
    providers: [MsgService,SnackbarComponent],
    bootstrap: [],
    entryComponents: [MsgAddComponent,MsgUpdateComponent,MsgDeleteComponent]
})
export class MessageLibModule { }
